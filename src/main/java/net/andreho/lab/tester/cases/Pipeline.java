package net.andreho.lab.tester.cases;

import java.util.List;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public interface Pipeline<T> {
   /**
    * @return immutable collection of elements in this pipeline
    */
   List<T> getElements();
}

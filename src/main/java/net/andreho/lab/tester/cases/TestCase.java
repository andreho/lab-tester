package net.andreho.lab.tester.cases;

import net.andreho.lab.tester.context.ClassFile;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public interface TestCase {
   /**
    * @return
    */
   String getDescription();

   /**
    * @return
    */
   String getFailureMessage();

   /**
    * @param classFile
    * @return
    */
   boolean test(ClassFile classFile);
}

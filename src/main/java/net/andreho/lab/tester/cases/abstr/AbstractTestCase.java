package net.andreho.lab.tester.cases.abstr;

import net.andreho.lab.tester.cases.TestCase;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public abstract class AbstractTestCase implements TestCase {
   public AbstractTestCase(String description, String failureMessage) {
      this.description = description;
      this.failureMessage = failureMessage;
   }

   //-----------------------------------------------------------------------------------------------------------------

   private final String description;
   private final String failureMessage;

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public String getDescription() {
      return description;
   }

   @Override
   public String getFailureMessage() {
      return failureMessage;
   }
}

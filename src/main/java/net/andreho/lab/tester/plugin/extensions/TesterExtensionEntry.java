package net.andreho.lab.tester.plugin.extensions;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public class TesterExtensionEntry {

   public TesterExtensionEntry() {
   }

   public TesterExtensionEntry(String name) {
      this.name = name;
   }

   //-----------------------------------------------------------------------------------------------------------------

   private String name;

   private String repository;

   //-----------------------------------------------------------------------------------------------------------------

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }
}

package net.andreho.lab.tester.plugin;

import net.andreho.lab.tester.plugin.extensions.TesterExtension;
import org.gradle.api.Plugin;
import org.gradle.api.Task;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.internal.reflect.DirectInstantiator;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public class TesterGradlePlugin implements Plugin<ProjectInternal>
{
   @Override
   public void apply(ProjectInternal project)
   {
      TesterExtension extension = (TesterExtension) project.getExtensions().findByName(TesterExtension.ALIAS);
      if (extension == null)
      {
         extension = project.getExtensions().create(
               TesterExtension.ALIAS, TesterExtension.class, DirectInstantiator.INSTANCE);
      }

      Task buildTask = project.getTasks().getByName("build");

   }
}

package net.andreho.lab.tester.plugin.extensions;

import groovy.lang.Closure;
import org.gradle.api.Namer;
import org.gradle.api.internal.DefaultNamedDomainObjectList;
import org.gradle.internal.reflect.Instantiator;
import org.gradle.util.Configurable;
import org.gradle.util.ConfigureUtil;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public class TesterExtension extends DefaultNamedDomainObjectList<TesterExtensionEntry> implements Configurable<TesterExtension> {

   public static final String ALIAS = "tester";

   //----------------------------------------------------------------------------------------------------------------

   public TesterExtension(Instantiator instantiator) {
      super(TesterExtensionEntry.class, instantiator, new EntryNamer());
   }

   private static final class EntryNamer implements Namer<TesterExtensionEntry> {
      @Override
      public String determineName(TesterExtensionEntry entry) {
         return entry.getName();
      }
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public TesterExtension configure(Closure closure) {
      return ConfigureUtil.configure(closure, this, false);
   }

   //-----------------------------------------------------------------------------------------------------------------
}

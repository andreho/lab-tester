package net.andreho.lab.tester.context;

import com.github.javaparser.ast.CompilationUnit;
import org.objectweb.asm.tree.ClassNode;

import java.util.Objects;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public class ClassFile {

   public ClassFile(Context context, String className) {
      this.context = Objects.requireNonNull(context);
      this.className = Objects.requireNonNull(className);
   }

   //-----------------------------------------------------------------------------------------------------------------

   private final Context context;
   private final String className;

   private Class<?> loadedClass;
   private ClassNode compiledClass;
   private CompilationUnit parsedClass;

   //-----------------------------------------------------------------------------------------------------------------

   public Context getContext() {
      return context;
   }

   public String getClassName() {
      return className;
   }

   public Class<?> getLoadedClass() {
      return loadedClass;
   }

   public void setLoadedClass(Class<?> loadedClass) {
      this.loadedClass = loadedClass;
   }

   public ClassNode getCompiledClass() {
      return compiledClass;
   }

   public void setCompiledClass(ClassNode compiledClass) {
      this.compiledClass = compiledClass;
   }

   public CompilationUnit getParsedClass() {
      return parsedClass;
   }

   public void setParsedClass(CompilationUnit parsedClass) {
      this.parsedClass = parsedClass;
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public String toString() {
      return className;
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      ClassFile classFile = (ClassFile) o;
      return Objects.equals(getClassName(), classFile.getClassName());
   }

   @Override
   public int hashCode() {
      return Objects.hashCode(getClassName());
   }
}

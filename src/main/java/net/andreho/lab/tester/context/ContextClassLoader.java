package net.andreho.lab.tester.context;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.jar.JarFile;

/**
 * Created by a.hofmann on 11.05.2016.
 */
public class ContextClassLoader extends ClassLoader {
   public ContextClassLoader(ClassLoader parent,
                             Collection<File> classPath,
                             Collection<JarFile> jarFiles) {
      super(parent);
      this.classPath = Collections.unmodifiableCollection(classPath);
      this.jarFiles = Collections.unmodifiableCollection(jarFiles);
      this.files = new TreeMap<>();
   }

   //-----------------------------------------------------------------------------------------------------------------

   private final Collection<File> classPath;
   private final Collection<JarFile> jarFiles;
   private final Map<String, File> files;

   private final ConcurrentMap<String, ClassFile> loadedClasses = new ConcurrentHashMap<>();

   //-----------------------------------------------------------------------------------------------------------------

   protected void initialize() {
      final StringBuilder builder = new StringBuilder();
      for (File directory : classPath) {
         scanDirectory(directory, builder);
      }
   }

   protected void scanDirectory(final File directory, final StringBuilder builder) {
      if (!directory.isDirectory()) {
         throw new IllegalStateException("Expected a directory, but got: " + directory);
      }

      builder.append('/').append(directory.getName());

      try {
         for (File file : directory.listFiles()) {
            if (file.isFile()) {
               files.put(builder + "/" + file.getName(), file);
            } else if (file.isDirectory()) {
               scanDirectory(file, builder);
            }
         }
      } finally {
         builder.setLength(builder.length() - (1 + directory.getName().length()));
      }
   }

   //-----------------------------------------------------------------------------------------------------------------

   public ClassFile resolve(String className) {
      return null;
   }

   //-----------------------------------------------------------------------------------------------------------------

   @Override
   protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
      Class<?> loadedClass = findClass(name);
      if (loadedClass != null) {
         return loadedClass;
      }

      return super.loadClass(name, resolve);
   }

   //-----------------------------------------------------------------------------------------------------------------
}
